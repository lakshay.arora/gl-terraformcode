resource "aws_lb_target_group" "alb-targetGroup" {
  vpc_id               = aws_vpc.VPC.id
  port                 = 80
  protocol_version     = "HTTP1"
  protocol             = "HTTP"
  target_type          = "instance"
  health_check {
    unhealthy_threshold = 2
    healthy_threshold   = 2
    timeout             = 3
    interval = 30
    path = "/"
    protocol = "HTTP"
  }
  tags = {
    Name = "tf_target_group"
  }
}

resource "aws_lb_target_group" "alb-targetGroup-internal" {
  vpc_id               = aws_vpc.VPC.id
  port                 = 80
  protocol_version     = "HTTP1"
  protocol             = "HTTP"
  target_type          = "instance"
  health_check {
    unhealthy_threshold = 2
    healthy_threshold   = 2
    timeout             = 3
    interval = 30
    path = "/"
    protocol = "HTTP"
  }
  tags = {
    Name = "tf_target_group-internal"
  }
}