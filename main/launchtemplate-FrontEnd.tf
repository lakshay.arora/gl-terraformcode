resource "aws_launch_template" "default_launch_template" {
  name = var.launch-template

  image_id      = var.image_id
  instance_type = var.instance_type
  key_name      = var.key_name
  vpc_security_group_ids = [aws_security_group.asg1-sg.id]
  tags = {
    Name = "EC2_instance_FrontEnd"
  }
}

#bashion host - Assigned to Rajat
#NAT - done
#route table and association - Done
#auto scaling - done
#launch tem backend - done
#ASG with lauchntemap - done
#4 security groups- done
#target-group: pending