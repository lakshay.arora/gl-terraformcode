resource "aws_autoscaling_group" "Backend-ASG-LaunchTem" {
  name                      = "terraform-test-backend"
  max_size                  = 2
  min_size                  = 1
  health_check_grace_period = 60 # default 5 mins
  health_check_type         = "ELB"
  desired_capacity          = 1
  force_delete              = true
    launch_template {
    id      = aws_launch_template.salary_launch_template.id
    version = "$Latest"
  }
  vpc_zone_identifier       = [aws_subnet.subnet1.id, aws_subnet.subnet1-AZ2.id]

  capacity_rebalance = false
  default_cooldown   = 120
  target_group_arns = [aws_lb_target_group.alb-targetGroup-internal.arn] #### test to attach target group #######
  termination_policies      = ["default"]
  wait_for_capacity_timeout = 0     #previously it was 3 #   Setting this to "0" causes Terraform to skip all Capacity Waiting behavior.
  #max_instance_lifetime     = 86450 ## must be between 86400 and 31536000 seconds

}