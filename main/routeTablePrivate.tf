resource "aws_route_table" "route_table-pvt" {
  vpc_id = aws_vpc.VPC.id
  tags   = var.tag-private
}

resource "aws_route_table_association" "rt-association-pvt" {
  route_table_id = aws_route_table.route_table-pvt.id
  subnet_id      = aws_subnet.subnet1.id
}

resource "aws_route_table_association" "rt-Association-pvt1" {
  route_table_id = aws_route_table.route_table.id
  subnet_id      = aws_subnet.subnet1-AZ2.id
}