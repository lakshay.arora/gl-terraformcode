resource "aws_eip" "eip" {
  vpc = true
}

resource "aws_nat_gateway" "ngw" {
  subnet_id     = aws_subnet.subnet2.id
  allocation_id = aws_eip.eip.id
}

resource "aws_route" "route" {
  route_table_id         = aws_route_table.route_table-pvt.id
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = aws_nat_gateway.ngw.id
}
