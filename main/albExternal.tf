resource "aws_lb" "ALB" {
  name               = var.ALB
  internal           = var.external
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb1-sg.id]
  subnets            = [aws_subnet.subnet2.id, aws_subnet.subnet2-AZ2.id]

  enable_deletion_protection = false
    
  tags = {
    Environment = "External"
  }
}

resource "aws_lb_listener" "listener" {
  load_balancer_arn = aws_lb.ALB.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb-targetGroup.arn
  }
}