output "vpc_id" {
  value = aws_vpc.VPC.id
}

output "vpc_cidr" {
  value = aws_vpc.VPC.cidr_block
}

output "launch_template_id" {
  value = aws_launch_template.default_launch_template.id
}