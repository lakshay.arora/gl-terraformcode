resource "aws_route_table" "route_table" {
  vpc_id = aws_vpc.VPC.id
  tags   = var.tags
}

resource "aws_route_table_association" "rt-association" {
  route_table_id = aws_route_table.route_table.id
  subnet_id      = aws_subnet.subnet2-AZ2.id
}

resource "aws_route_table_association" "rt-Association" {
  route_table_id = aws_route_table.route_table.id
  subnet_id      = aws_subnet.subnet2.id
}

