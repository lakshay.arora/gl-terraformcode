resource "aws_lb" "alb" {
  name               = var.alb
  internal           = var.internal
  load_balancer_type = "application"
  security_groups    = [aws_security_group.alb2-sg.id]
  subnets            = [aws_subnet.subnet1.id, aws_subnet.subnet1-AZ2.id]

  enable_deletion_protection = false

  tags = {
    Environment = "Internal"
  }
}

resource "aws_lb_listener" "listener-internal" {
  load_balancer_arn = aws_lb.alb.arn
  port              = "80"
  protocol          = "HTTP"
  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.alb-targetGroup-internal.arn
  }
}