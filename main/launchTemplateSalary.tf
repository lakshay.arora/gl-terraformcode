resource "aws_launch_template" "salary_launch_template" {
  name = var.salary-launch-template
  image_id      = var.image_id_salary
  instance_type = var.instance_type_salary
  key_name      = var.key_name_Salary
    vpc_security_group_ids = [aws_security_group.asg2-sg.id]
  tags = {
    Name = "EC2_instance_Salary"
  }
}
