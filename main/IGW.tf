resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.VPC.id

}

resource "aws_route" "routing-igw" {
  route_table_id         = aws_route_table.route_table.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.igw.id
}